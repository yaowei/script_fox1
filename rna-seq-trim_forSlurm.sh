#!/usr/bin/env bash

project_dir=nvrm_fox1
script_dir=script_fox1

mkdir /n/scratch3/users/y/yl477/${project_dir}/fastqc_trim
mkdir /n/scratch3/users/y/yl477/${project_dir}/trim_galore
source /programs/biogrids.shrc

for fq1 in /n/scratch3/users/y/yl477/${project_dir}/fastq/*_pass_1.fastq.gz; do
fq2=$(echo $fq1 | sed 's/_pass_1.fastq.gz/_pass_2.fastq.gz/g');

sbatch -p short -t 0-3:00 -n 8 --mem 16G --job-name rnaseq-trim --wrap="sh /n/scratch3/users/y/yl477/${project_dir}/${script_dir}/rna-seq-trim.sh $fq1 $fq2"
sleep 1	# wait 1 second between each job submission

done
