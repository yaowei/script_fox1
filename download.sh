#!/bin/bash

#SBATCH -c 8
#SBATCH -N 1

#SBATCH -p short
#SBATCH --job-name SRA
#SBATCH --mem 16gb
#SBATCH --time 0-6:00

module load sratoolkit/2.10.7

echo /repository/user/main/public/root = \"/n/scratch3/users/y/yl477/ncbi\" >> ~/.ncbi/user-settings.mkfg

prefetch --max-size 40G SRR2426752 SRR2426753 SRR2426754 SRR2426755 SRR2426756 SRR2426757

vdb-validate SRR15130447 SRR2426752 SRR2426753 SRR2426754 SRR2426755 SRR2426756 SRR2426757
{"mode":"full","isActive":false}