#!/bin/bash/

project_dir=nvrm_fox1
script_dir=script_fox1

sra=$1
tmp_dir=/n/scratch3/users/y/yl477/${project_dir}/tmp
out_dir=/n/scratch3/users/y/yl477/${project_dir}/fastq

# grab base of filename for naming outputs

base=`basename $sra .subset.sra`
echo "Sample name is $base"           


python3 /home/yl477/software/parallel-fastq-dump/parallel-fastq-dump --threads 8 --tmpdir ${tmp_dir} --outdir ${out_dir} --gzip --skip-technical  --readids --read-filter pass --dumpbase --split-files --clip --sra-id $sra
