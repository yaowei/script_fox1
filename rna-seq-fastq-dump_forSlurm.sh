#! /bin/bash
source /programs/biogrids.shrc
module load gcc/6.2.0
module load python/3.7.4
module load sratoolkit/2.10.7
mkdir /n/scratch3/users/y/yl477/dox_heart/tmp

project_dir=nvrm_fox1
script_dir=script_fox1

mkdir /n/scratch3/users/y/yl477/${project_dir}/tmp
mkdir /n/scratch3/users/y/yl477/${project_dir}/fastq

for sra in /n/scratch3/users/y/yl477/${project_dir}/sra/*.sra
do

sbatch -p short -t 0-6:00 -n 8 --mem 16G --job-name fastq-dump --wrap="sh /n/scratch3/users/y/yl477/${project_dir}/${script_dir}/rna-seq-fastq-dump.sh $sra"
sleep 1	# wait 1 second between each job submission
  
done