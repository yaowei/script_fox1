#!/usr/bin/env bash

# This script takes a fastq file of RNA-Seq data, runs FastQC and outputs a counts file for it.
# USAGE: sh rnaseq_analysis_on_allfiles.sh <name of fastq file>

# initialize a variable with an intuitive name to store the name of the input fastq file

bam=$1

gtf=/home/yl477/Genome_files/rat_genome/Rattus_norvegicus.Rnor_6.0.90.gtf
#gtf=/home/yl477/Genome_files/human_genome/Homo_sapiens.GRCh38.97.gtf
#gtf=/home/yl477/Genome_files/mouse_genome/Mus_musculus.GRCm38.102.gtf
#galias=grch38
galias=rn6
P=4

# grab base of filename for naming outputs
base=$(basename ${bam} |cut -f 1 -d "_");
echo "Sample name is $base"           

# Run initial Stringtie to get assembly from each sample

stringtie ${bam} -G ${gtf} -e -B -p ${P} -o ../StringTie/stringtie_out_refonly_e/${base}/${base}_out.gtf -A ../StringTie/stringtie_out_refonly_e/${base}/${base}_gene_abund.tab
echo "DONE"
